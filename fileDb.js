const fs = require('fs');

const date = new Date();
const strDate = date.toISOString();
const path = "./messages";
const data =[];

module.exports = {
	init: () => {
		return new Promise((resolve, reject) => {
			fs.readdir(path, (err, files) => {
				files.forEach(file => {
					fs.readFile(`${path}/${file}`, (err, res) => {
						if (err) {
							throw err;
						}
						data.push(JSON.parse(res));
					});
				});
			});
			resolve();
		});
	},
	getData: () => {
		let res = [];
		for (let i = 0; i < 5; i++){
			res.push(data[i])
		}
		return res
	},
	
	addItem: (item) => {
		data.push(item);
		return new Promise((resolve, reject) => {
			let dat = {...item, datetime: strDate};
		fs.writeFile(`./messages/${Date.now()}.txt`, JSON.stringify(dat), err => {
			if (err) {
				throw err;
				} else {
					resolve(item);
				}
			});
		});
	}
};